package com.rachidi.kata.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rachidi.kata.dto.CommandeDto;
import com.rachidi.kata.exceptions.APIErrorException;
import com.rachidi.kata.services.CommandeService;


@RestController
@RequestMapping("api/commande")
public class CommandeController {
	
	@Autowired
    CommandeService commandeService;

    @PostMapping(path = "/")
    public ResponseEntity<CommandeDto> createCommande(@RequestBody(required = false) CommandeDto commande) throws APIErrorException {
    	commandeService.create(commande);
    	
    	return ResponseEntity.ok(commandeService.create(commande));
    }
    
    @PostMapping(path = "/choose-delivery")
    public ResponseEntity<CommandeDto> chooseDeliveryMode(@RequestBody(required = false) CommandeDto commande)throws APIErrorException {
    	
    	return ResponseEntity.ok(commandeService.chooseDeliveryMode(commande));
    }
    
    @PostMapping(path = "/choose-time-slot")
    public ResponseEntity<CommandeDto> chooseTimeSlot(@RequestBody(required = false) CommandeDto commande)throws APIErrorException {
    	
    	return ResponseEntity.ok(commandeService.chooseTimeSlot(commande));
    }

}
