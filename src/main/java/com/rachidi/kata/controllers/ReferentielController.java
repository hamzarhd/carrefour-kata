package com.rachidi.kata.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rachidi.kata.enums.DeliveryMode;
import com.rachidi.kata.services.CommandeService;

@RestController
@RequestMapping("api/referentiel")
public class ReferentielController {
	
	@Autowired
	CommandeService commandeService;
	
	 @GetMapping(path = "/delivery-mode")
	    public ResponseEntity<List<DeliveryMode>> getAvailableDeliveryMode() {
	    	
	    	return ResponseEntity.ok(commandeService.getAvailableDeliveryMode());
	    }

}
