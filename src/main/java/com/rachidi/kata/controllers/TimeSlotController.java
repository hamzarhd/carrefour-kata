package com.rachidi.kata.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rachidi.kata.dto.CommandeDto;
import com.rachidi.kata.dto.TimeSlotDto;
import com.rachidi.kata.exceptions.APIErrorException;
import com.rachidi.kata.services.TimeSlotService;

@RestController
@RequestMapping("api/time-slot")
public class TimeSlotController {
	
	@Autowired
	TimeSlotService timeSlotService;
	
	 @PostMapping(path = "/")
	    public ResponseEntity<List<TimeSlotDto>> getAvailableTimeSlots(@RequestBody(required = false) CommandeDto commande) throws APIErrorException{
	    	
	    	return ResponseEntity.ok(timeSlotService.getAvailableTimeSlot(commande));
	    }


}
