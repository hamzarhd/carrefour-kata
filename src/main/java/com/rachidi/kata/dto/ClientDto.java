package com.rachidi.kata.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ClientDto {
	
	private Long id;
	
	private String firstName;
	
	private String lastName;
	
	private String email;
	
	private String phone;

}
