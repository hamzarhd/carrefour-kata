package com.rachidi.kata.dto;

import com.rachidi.kata.enums.DeliveryMode;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CommandeDto {
	
	private Long id;
	
	private ClientDto client;
	
	private DeliveryMode deliveryMode;
	
	private TimeSlotDto timeSlot;

}
