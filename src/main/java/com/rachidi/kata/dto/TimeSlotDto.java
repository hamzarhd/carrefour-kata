package com.rachidi.kata.dto;

import com.rachidi.kata.enums.DeliveryMode;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TimeSlotDto {
	
	private Long id;
	
	private String day;
	
	private String time;
	
	private boolean isReserved;
	
	private DeliveryMode deliveryMode;

}
