package com.rachidi.kata.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.rachidi.kata.exceptions.APIErrorException;
import com.rachidi.kata.exceptions.ApiError;
import com.rachidi.kata.exceptions.ErrorCode;
import com.rachidi.kata.exceptions.NotFoundIdException;

import java.util.*;

@ControllerAdvice
public class CommonControllerAdvice {

    @Autowired
    private com.rachidi.kata.exceptions.ExceptionHandler exceptionHandler;

	@ExceptionHandler({ NotFoundIdException.class })
	public ResponseEntity<Object> handle(NotFoundIdException e) {
		exceptionHandler.handle(e);
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getApiError());

	}


	@ExceptionHandler({ APIErrorException.class })
	public ResponseEntity<Object> handle(APIErrorException e) {
		exceptionHandler.handle(e);
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getApiError());

	}

	@ExceptionHandler({ Exception.class })
	public ResponseEntity<Object> handle(Exception e) {
		String uuid = UUID.randomUUID().toString();
		exceptionHandler.handle(e, uuid);
		ErrorCode code = ErrorCode.A500;
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ApiError(code, code.toString(), uuid));

	}
	
}