package com.rachidi.kata.config;

import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.servers.Server;

@Configuration
public class SwaggerConfig {
	
	 @Bean
	  public OpenAPI myOpenAPI() {
	 Server devServer = new Server();
	    devServer.setUrl("http://localhost:8090");
	    devServer.setDescription("Server URL in Development environment");

	    Contact contact = new Contact();
	    contact.setEmail("hamzaa.rachidii@gmail.com");
	    contact.setName("Hamza Rachidi");

	    License mitLicense = new License().name("My License").url("https://mylicense.com/");

	    Info info = new Info()
	        .title("Tutorial Management API")
	        .version("1.0")
	        .contact(contact)
	        .description("This api is an exercise kata for Carrefour.").termsOfService("http://localhost:8090")
	        .license(mitLicense);

	    return new OpenAPI().info(info).servers(List.of(devServer));
	  }
   
}