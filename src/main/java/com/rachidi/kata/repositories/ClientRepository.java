package com.rachidi.kata.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rachidi.kata.model.Client;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long>{

}
