package com.rachidi.kata.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.rachidi.kata.enums.DeliveryMode;
import com.rachidi.kata.model.TimeSlot;

@Repository
public interface TimeSlotRepository extends JpaRepository<TimeSlot, Long>{
	
	@Query("FROM TimeSlot t WHERE t.deliveryMode = :deliveryMode and isReserved = false ")
	List<TimeSlot> findAvailableTimeSlotByDeliveryMode(@Param("deliveryMode") DeliveryMode deliveryMode);

}
