package com.rachidi.kata.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rachidi.kata.model.Commande;

@Repository
public interface CommandeRepository extends JpaRepository<Commande, Long>{

}
