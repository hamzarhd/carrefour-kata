package com.rachidi.kata.enums;

public enum DeliveryMode {

	DRIVE, DELIVERY, DELIVERY_TODAY, DELIVERY_ASAP;
}
