package com.rachidi.kata.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rachidi.kata.dto.CommandeDto;
import com.rachidi.kata.dto.TimeSlotDto;
import com.rachidi.kata.enums.DeliveryMode;
import com.rachidi.kata.exceptions.APIErrorException;
import com.rachidi.kata.exceptions.ErrorCode;
import com.rachidi.kata.mappers.TimeSlotMapper;
import com.rachidi.kata.model.Commande;
import com.rachidi.kata.model.TimeSlot;
import com.rachidi.kata.repositories.CommandeRepository;
import com.rachidi.kata.repositories.TimeSlotRepository;

@Service
public class TimeSlotService {
	
	@Autowired
	TimeSlotMapper timeSlotMapper;
	
	@Autowired
	CommandeRepository commandeRepository;
	
	@Autowired
	TimeSlotRepository timeSlotRepository;

	//get available time slots
	public List<TimeSlotDto> getAvailableTimeSlot(CommandeDto commande) throws APIErrorException{
		
		Long idCommande = Optional.ofNullable(commande).map(CommandeDto::getId).orElse(0l);
		DeliveryMode deliveryMode= Optional.ofNullable(commande).map(CommandeDto::getDeliveryMode).orElse(null);
		// check if id commande exist
		if(idCommande == 0) throw new APIErrorException(ErrorCode.A300);
		
		Optional<Commande> commandeOp = commandeRepository.findById(idCommande);
		
		//check if commande exist
		if(!commandeOp.isPresent())
			throw new APIErrorException(ErrorCode.A302);
		
		Commande commandeEntity = commandeOp.get();
		
		if(commandeEntity.getDeliveryMode() == null ) throw new APIErrorException(ErrorCode.A301);
		
		List<TimeSlot> timeSlots = timeSlotRepository.findAvailableTimeSlotByDeliveryMode(commandeEntity.getDeliveryMode());
				
		return timeSlotMapper.timeSlotEentityListToDto(timeSlots);
		
	}
}
