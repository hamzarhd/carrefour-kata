package com.rachidi.kata.services;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rachidi.kata.dto.ClientDto;
import com.rachidi.kata.dto.CommandeDto;
import com.rachidi.kata.dto.TimeSlotDto;
import com.rachidi.kata.enums.DeliveryMode;
import com.rachidi.kata.exceptions.APIErrorException;
import com.rachidi.kata.exceptions.ErrorCode;
import com.rachidi.kata.mappers.CommandeMapper;
import com.rachidi.kata.model.Client;
import com.rachidi.kata.model.Commande;
import com.rachidi.kata.model.TimeSlot;
import com.rachidi.kata.repositories.ClientRepository;
import com.rachidi.kata.repositories.CommandeRepository;
import com.rachidi.kata.repositories.TimeSlotRepository;

@Service
public class CommandeService {

	@Autowired
	CommandeMapper commandeMapper;
	
	@Autowired
	CommandeRepository commandeRepository;
	
	@Autowired
	ClientRepository clientRepository;
	
	@Autowired
	TimeSlotRepository timeSlotRepository;
	
	
	// get available delivery mode
	public List<DeliveryMode> getAvailableDeliveryMode() {
		return Arrays.asList(DeliveryMode.values());
	}
	
	//choose delivery mode
	public CommandeDto chooseDeliveryMode(CommandeDto commande) throws APIErrorException {
		
		Long idCommande = Optional.ofNullable(commande).map(CommandeDto::getId).orElse(0l);
		DeliveryMode deliveryMode= Optional.ofNullable(commande).map(CommandeDto::getDeliveryMode).orElse(null);
		// check if id exist
		if(idCommande == 0) throw new APIErrorException(ErrorCode.A300);
		
		//check if delivery mode exist
		if(deliveryMode == null) throw new APIErrorException(ErrorCode.A301);
		
		Optional<Commande> commandeOp = commandeRepository.findById(idCommande);
		
		//check if commande exist
		if(!commandeOp.isPresent())
			throw new APIErrorException(ErrorCode.A302);
		
		Commande commandeEntity = commandeOp.get();
		commandeEntity.setDeliveryMode(deliveryMode);
		commandeRepository.save(commandeEntity);
		return commandeMapper.commandeEntityToDto(commandeEntity);
	}
	
	
	//choose time slot
	public CommandeDto chooseTimeSlot(CommandeDto commande) throws APIErrorException {
		
		
		Long idCommande = Optional.ofNullable(commande).map(CommandeDto::getId).orElse(0l);
		Long idTimeSlot = Optional.ofNullable(commande).map(CommandeDto::getTimeSlot).map(TimeSlotDto::getId).orElse(0l);
		// check if id exist
		if(idCommande == 0) throw new APIErrorException(ErrorCode.A300);
		
		//check if delivery mode exist
		if(idTimeSlot == 0) throw new APIErrorException(ErrorCode.A600);
		
		Optional<Commande> commandeOp = commandeRepository.findById(idCommande);
		
		//check if commande exist
		if(!commandeOp.isPresent())
			throw new APIErrorException(ErrorCode.A302);
		
		Optional<TimeSlot> timeSlotOp = timeSlotRepository.findById(idTimeSlot);
		
		if(!timeSlotOp.isPresent()) throw new APIErrorException(ErrorCode.A601);
		
		TimeSlot timeSlot = timeSlotOp.get();
		
		if(Boolean.TRUE.equals(timeSlot.getIsReserved())) throw new APIErrorException(ErrorCode.A602);
		
		Commande commandeEntity = commandeOp.get();
		
		commandeEntity.setTimeSlot(timeSlot);
		commandeRepository.save(commandeEntity);
		
		timeSlot.setIsReserved(true);
		timeSlotRepository.save(timeSlot);
		return commandeMapper.commandeEntityToDto(commandeEntity);
	}

	public CommandeDto create(CommandeDto commande) throws APIErrorException{
		
		Long idClient = Optional.ofNullable(commande).map(CommandeDto::getClient).map(ClientDto::getId).orElse(0l);

		// check if id exist
		if(idClient == 0) throw new APIErrorException(ErrorCode.A400);
		
		Optional<Client> clientOp = clientRepository.findById(idClient);
		
		//check if commande exist
		if(!clientOp.isPresent())
			throw new APIErrorException(ErrorCode.A401);
		
		
		Commande commandeEntity = new Commande();
		commandeEntity.setClient(clientOp.get());

		return commandeMapper.commandeEntityToDto(commandeEntity);
	}
}
