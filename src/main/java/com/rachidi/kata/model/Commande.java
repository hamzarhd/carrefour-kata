package com.rachidi.kata.model;

import com.rachidi.kata.enums.DeliveryMode;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "commande")
@Entity
public class Commande {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    @Setter(AccessLevel.NONE)
	private Long id;
	
	
	@ManyToOne
	@JoinColumn(name = "client_id")
	private Client client;
	
	@Column
	private DeliveryMode deliveryMode;
	
	@OneToOne
	@JoinColumn(name = "time_slot_id")
	private TimeSlot timeSlot;
	
	//other fields
}
