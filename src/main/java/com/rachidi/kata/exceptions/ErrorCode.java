package com.rachidi.kata.exceptions;

public enum ErrorCode {

	A001(""), A500("Une erreur système s'est produite"),A404("Saisie non valide !"), A300("id commande manquant"),A301("mode livraison manquant"),
	A302("commande non trouvé"),A400("id Client manquant"),A401("Client non trouvé"),A600("id Creneau manquant"),
	A601("Creneau non trouvé"), A602("Creneau déjà reservé");

	private String code = "";

	ErrorCode(String code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return code;
	}
}
