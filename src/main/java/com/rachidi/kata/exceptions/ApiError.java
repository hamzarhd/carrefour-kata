package com.rachidi.kata.exceptions;

import java.io.Serializable;
import java.util.List;

public class ApiError implements Serializable {

	private static final long serialVersionUID = -4130929122515363388L;

	private ErrorCode code;
	private String description;
	private String uuid;
	private List<String> fieldErrors;

	public ApiError(ErrorCode code, String description, String uuid) {
		this.code = code;
		this.description = description;
		this.uuid = uuid;
	}

	public ApiError(ErrorCode code, String description) {
		this.code = code;
		this.description = description;
	}

	public ApiError(ErrorCode code, List<String> fieldErrors) {
		this.code = code;
		this.fieldErrors = fieldErrors;
	}

	public ErrorCode getCode() {
		return code;
	}

	public void setCode(ErrorCode code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<String> getFieldErrors() {
		return fieldErrors;
	}

	public void setFieldErrors(List<String> fieldErrors) {
		this.fieldErrors = fieldErrors;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public static void manageExceptions(String errors) throws APIErrorException {

		// manage exception
	}
}
