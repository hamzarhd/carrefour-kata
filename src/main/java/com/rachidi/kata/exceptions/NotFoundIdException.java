package com.rachidi.kata.exceptions;

public class NotFoundIdException extends APIErrorException {


    public NotFoundIdException(ErrorCode code) {
        super(code);
    }


}
