package com.rachidi.kata.mappers;


import java.util.List;

import org.mapstruct.Mapper;

import com.rachidi.kata.dto.CommandeDto;
import com.rachidi.kata.model.Commande;

@Mapper(componentModel = "spring")
public interface CommandeMapper {

    Commande commandeDtoToEntity(CommandeDto commande);

    CommandeDto commandeEntityToDto(Commande commande);

    List<Commande> commandeDtoListToEntity(List<CommandeDto> commandes);

    List<CommandeDto> commandeEentityListToDto(List<Commande> commandes);
}
