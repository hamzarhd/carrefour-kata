package com.rachidi.kata.mappers;


import java.util.List;

import org.mapstruct.Mapper;

import com.rachidi.kata.dto.ClientDto;
import com.rachidi.kata.model.Client;

@Mapper(componentModel = "spring")
public interface ClientMapper {

    Client clientDtoToEntity(ClientDto client);

    ClientDto clientEntityToDto(Client client);

    List<Client> clientDtoListToEntity(List<ClientDto> clients);

    List<ClientDto> clientEentityListToDto(List<Client> clients);
}
