package com.rachidi.kata.mappers;

import java.util.List;

import org.mapstruct.Mapper;

import com.rachidi.kata.dto.TimeSlotDto;
import com.rachidi.kata.model.TimeSlot;


@Mapper(componentModel = "spring")
public interface TimeSlotMapper {

	TimeSlot timeSlotDtoToEntity(TimeSlotDto timeSlot);

	TimeSlotDto timeSlotEntityToDto(TimeSlot timeSlot);

    List<TimeSlot> timeSlotDtoListToEntity(List<TimeSlotDto> timeSlots);

    List<TimeSlotDto> timeSlotEentityListToDto(List<TimeSlot> timeSlots);
}
